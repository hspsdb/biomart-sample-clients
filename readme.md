
Sample scripts to call genenames.org Biomart xml query and REST services to get gene names
and symbols for a given list of Ensembl gene ids.
